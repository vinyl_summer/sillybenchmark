import argparse
import time
from multiprocessing import Process, Queue
from typing import Callable

from sillyBenchmark import setup, naiveApproach, memoryOptimized, cleanup


def parse_arguments() -> int:
    parser = argparse.ArgumentParser(
        description='Benchmark naive integer sum from file implementation vs a memory optimized one'
    )
    parser.add_argument(
        'number',
        metavar='N',
        type=int,
        help='number of random numbers to sum (in power of 2)'
    )

    args = parser.parse_args()
    number_of_numbers: int = 2 ** args.number

    return number_of_numbers


def benchmark_approach(approach: Callable[[Queue], None]) -> (int, float, float):
    q = Queue()
    p = Process(target=approach, args=(q,))

    start = time.monotonic()
    p.start()
    p.join()
    end = time.monotonic()

    elapsed = end - start
    results, memory_used = q.get()

    return results, memory_used, elapsed


number_of_numbers: int = parse_arguments()
setup.create_numbers_file(number_of_numbers)

results, memory_used, elapsed = benchmark_approach(naiveApproach.run)
print(f"Naive approach:\n {results=},\n {memory_used=},\n {elapsed=}")

results, memory_used, elapsed = benchmark_approach(memoryOptimized.run)
print(f"Memory optimized approach:\n {results=},\n {memory_used=},\n {elapsed=}")

cleanup.delete_numbers_file()
