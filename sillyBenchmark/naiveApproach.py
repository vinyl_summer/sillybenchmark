from multiprocessing import Queue

import psutil

from sillyBenchmark import constants


def run(q: Queue) -> None:
    with open(constants.NUMBERS_FILE_PATH, 'r', encoding="UTF-8") as file:
        numbers = [int(i) for i in file.read().split()]
    number_sum = sum(numbers)

    q.put((number_sum, psutil.Process().memory_info().rss / 1024 ** 2))
    return
