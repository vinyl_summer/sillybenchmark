from multiprocessing import Queue

from sillyBenchmark import constants

import psutil


def run(q: Queue) -> None:
    with open(constants.NUMBERS_FILE_PATH, 'r', encoding="UTF-8") as file:
        character_buffer = []
        digit: int
        number_sum = 0
        while True:
            character = file.read(1)
            if not character:
                if character_buffer:
                    digit = int(''.join(character_buffer))
                    number_sum += digit
                break

            if character == ' ':
                if character_buffer:
                    digit = int(''.join(character_buffer))
                    number_sum += digit
                    character_buffer = []
            else:
                character_buffer.append(character)

    q.put((number_sum, psutil.Process().memory_info().rss / 1024 ** 2))
    return
