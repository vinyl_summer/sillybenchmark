from sillyBenchmark import constants
import logging

logger = logging.getLogger()


def create_numbers_file(number_of_numbers: int) -> None:
    file_path: str = constants.NUMBERS_FILE_PATH
    logger.info(f"Writing {number_of_numbers} numbers to {file_path}")

    numbers: str = "1 " * number_of_numbers
    with open(file_path, "w") as file:
        file.write(numbers)

    logger.info("Successfully wrote the numbers to the file")
