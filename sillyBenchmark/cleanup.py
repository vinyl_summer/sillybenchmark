from sillyBenchmark import constants
import os
import logging

logger = logging.getLogger()


def delete_numbers_file() -> None:
    file_path: str = constants.NUMBERS_FILE_PATH
    logger.info(f"Cleaning up..")

    os.remove(file_path)

    logger.info("Successfully cleaned up")
