import logging

__all__ = ['cleanup', 'constants', 'naiveApproach', 'memoryOptimized', 'setup']

logging.basicConfig(
    format='[%(asctime)s] '
           'Thread: %(threadName)s, '
           'module: %(module)s '
           'at %(funcName)s '
           '(line %(lineno)s) '
           '- %(levelname)s - '
           '%(message)s',
    level=logging.INFO
)
