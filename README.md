# Silly Benchmark
Using this package, you can benchmark the difference between numbers addition
from file using a naive 3-line "pythonic" approach versus an optimized 17-line one.

List of assumptions I made:
1) The text file is encoded in UTF-8
2) All the numbers are integers 
3) All the numbers are separated by spaces
4) There are no special characters

The naive approach implementation:
1) Open the file, read all the content
2) Split the content by space into a list of integers
3) Sum the list

The optimized approach implementation:
1) Read the file byte by byte (character by character)
2) If it's a digit, add it to character buffer
3) If it's a space or EOF (no character), flush the buffer into sum variable 

TLDR (too lazy didn't run): memory optimized approach is slower, but provides constant memory usage (around 12 MB)
naive approach is much faster, but is an absolute memory hog. 

Both approaches scale linearly

## Usage
1) Clone the project to your machine:
```shell
https://gitlab.com/vinyl_summer/sillybenchmark.git
```

2) Create a python virtual environment and activate it:
```shell
python -m venv /path/to/new/virtual/environment
```
```shell
source <new_venv_path>/bin/activate
```

3) Install project requirements
```shell
pip install -r requirements.txt
```

3) Run the package 
```shell
python -m sillyBenchmark --help
```

## Example usage
```shell
python -m sillyBenchmark 25
```
Creates a txt file with 2 ** 25 "1"s separated by spaces, then runs the benchmark
